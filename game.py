def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(num):
    print_board(board)
    print("GAME OVER\n")
    print(board[num], "has won")
    exit()

def is_row_winner(board, num):
    if board[num] == board[num + 1] and board[num + 1] == board[num + 2]:
        return True

def is_column_winner(board, num):
    if board[num] == board[num + 3] and board[num + 3] == board[num + 6]:
        return True

def is_diagonal_winner(board, num, factor):
    if board[num] == board[num + (2 * factor)] and board[num + (2 * factor)] == board[num + (4 * factor)]:
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]

current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 0):
        game_over(0)
    elif is_row_winner(board, 3):
        game_over(3)
    elif is_row_winner(board, 6):
        game_over(6)
    elif is_column_winner(board, 0):
        game_over(0)
    elif is_column_winner(board, 1):
        game_over(1)
    elif is_column_winner(board, 2):
        game_over(2)
    elif is_diagonal_winner(board, 0, 2):
        game_over(0)
    elif is_diagonal_winner(board, 2, 1):
        game_over(2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
